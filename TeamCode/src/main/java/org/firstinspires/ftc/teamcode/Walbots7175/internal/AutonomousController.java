package org.firstinspires.ftc.teamcode.Walbots7175.internal;

/**
 * Created by nicolas on 1/28/18 in ftc_app.
 * <p>
 * Copyright (c) ©2018 Nicolas Hohaus
 * Copyright (c) ©2018 Walbots (7175)
 * <p>
 * Resource: https://gitlab.com/roboticsclub/ftc_app
 * Contact: nico@walbots.com, team@walbots.com
 */


/**
 * The class AutonomousController is designed to control a set of AutonomousFunctions and their
 * execution in a standardized way. A set of actions (AutonomousFunctions) have to be instantiated
 * and configured so that they can be passed to the AutonomousController. Then the autonomous
 * controller takes care about starting and running all these little actions in the right order and
 * in the right way.
 */
public class AutonomousController
{
    private AutonomousAction[] autonomousActions;   //All AutonomousFunctions to run (ordered)
    private LoggingController loggingController; //for displaying logs on the phone

    /**
     * Instantiates an AutonomousController with an ordered array of all AutonomousFunctions
     * that the controller should iterate through and the current running autonomous mode (OpMode)
     *
     * @param functionArray ordered array of all AutonomousFunctions to run
     */
    public AutonomousController(AutonomousAction[] functionArray)
    {
        autonomousActions = functionArray;
        loggingController = LoggingController.getCurrentInstance();
    }

    /**
     * The runAllFunctions() method runs all AutonomousFunctions one by one going through their
     * life-cycle (initialize(), run(), shutdown()). The user is notified by logs on the phone about
     * the current status and the AutonomousAction that is running.
     *
     * @throws InterruptedException When Thread.sleep is interrupted
     */
    public void runAllFunctions() throws InterruptedException
    {
        for (AutonomousAction autonomousAction : autonomousActions)
        {
            //Initialization
            autonomousAction.initialize();

            loggingController.showLog(autonomousAction.name, "Running....");

            //Running
            while (autonomousAction.run() == AutonomousAction.ActionState.RUNNING) {}

            //Waiting
            Thread.sleep(autonomousAction.timeToWaitAfterFinished);

            //Shutting down
            autonomousAction.shutdown();

            loggingController.showLog(autonomousAction.name, "Completed...");
        }
    }
}

